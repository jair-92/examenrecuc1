package com.example.examenrecuc1java;
import java.util.Random;

public class Cotizacion {
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porEnganche;
    private int plazo;

    //Constructor
    public Cotizacion(int folio, String descripcion, float valorAuto, float porEnganche, int plazo){
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porEnganche = porEnganche;
        this.plazo = plazo;

    }

    //Set y Get
    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorEnganche() {
        return porEnganche;
    }

    public void setPorEnganche(float porEnganche) {
        this.porEnganche = porEnganche;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    //Métodos
    public int generarFolio(int folio){
        //Generar número aleatorio
        Random random = new Random();
        int numeroAleatorio = random.nextInt(1000);
        return folio + numeroAleatorio;

    }

    public float calcularEnganche(float valorAuto, float porEnganche){
        float enganche =  (float) (valorAuto * porEnganche);
        return enganche;

    }

    public float calcularPagoMensual(float valorAuto, int plazo){
        float pagoMensual = (float) (valorAuto / plazo);
        return pagoMensual;

    }

}
