package com.example.examenrecuc1java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtCliente;
    private Button btnCotizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnCotizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cotizacion();
            }
        });

    }
    private void iniciarComponentes() {
        txtCliente = findViewById(R.id.txtCliente);
        btnCotizacion = findViewById(R.id.btnCotizacion);
    }

    private void cotizacion() {

        if(txtCliente.getText().toString().isEmpty()) {
            Toast.makeText(this.getApplicationContext(), "Faltan datos", Toast.LENGTH_SHORT).show();

        } else {
            // Hacer el paquete para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtCliente.getText().toString());

            // Crear el intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no una respuesta
            startActivity(intent);

        }
    }
}