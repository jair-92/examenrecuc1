package com.example.examenrecuc1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
    private TextView lblNombre;

    private TextView lblFolio;
    private EditText txtDescripcion;
    private EditText txtValorAuto;
    private EditText txtPorEng;

    private RadioButton rdb12;
    private RadioButton rdb18;
    private RadioButton rdb24;
    private RadioButton rdb36;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private EditText txtPagoMensual;
    private EditText txtEnganche;


    private Cotizacion Cot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();
        // Obtener los datos del MainActiviy
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        generarFol();
        lblNombre.setText(nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void iniciarComponentes() {
        // Relacionar los objetos
        lblNombre = findViewById(R.id.lblNombre);
        lblFolio = findViewById(R.id.lblFolio);
        txtDescripcion = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtPorEng = findViewById(R.id.txtPorEng);

        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb24 = findViewById(R.id.rdb24);
        rdb36 = findViewById(R.id.rdb36);


        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        txtPagoMensual = findViewById(R.id.txtPagoMensual);
        txtEnganche = findViewById(R.id.txtEnganche);



        Cot = new Cotizacion(0, "", 0, 0, 0);
    }

    public void generarFol(){
        double numeroAleatorio = Math.random();

        int minimo = 1;
        int maximo = 1000;
        int rango = maximo - minimo + 1;
        int folio= (int)(numeroAleatorio * rango) + minimo;
        lblFolio.setText(String.valueOf(folio));

    }


    private void calcular(){
        int porcentaje = 0;

        if(rdb12.isChecked()){
            porcentaje = 12;
        }else if(rdb18.isChecked()){
            porcentaje = 18;
        }else if(rdb24.isChecked()){
            porcentaje = 24;
        }else if(rdb36.isChecked()){
            porcentaje = 36;
        }

        if(txtDescripcion.getText().toString().isEmpty() || txtValorAuto.getText().toString().isEmpty()
                || txtPorEng.getText().toString().isEmpty()){
            Toast.makeText(this, "Ingrese los datos faltantes", Toast.LENGTH_SHORT).show();
        }else{
            float valorAuto = Float.parseFloat(txtValorAuto.getText().toString());
            float porEnganche = Float.parseFloat(txtPorEng.getText().toString());
            float enganche = Cot.calcularEnganche(valorAuto, porEnganche);
            txtEnganche.setText("" + enganche);

            float pagoMensual = Cot.calcularPagoMensual(valorAuto, enganche, porcentaje);
            txtPagoMensual.setText("" + pagoMensual);
        }



    }

    private void limpiar(){
        txtDescripcion.setText("");
        txtValorAuto.setText("");
        txtPorEng.setText("");
        txtPagoMensual.setText("");
        txtEnganche.setText("");

    }

    private void regresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cotizacion");
        confirmar.setMessage("Regresar a menu principal");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No se realiza nada
            }
        });
        confirmar.show();
    }
}